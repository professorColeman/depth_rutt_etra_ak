# Depth Based Rutt-Etra Example for Azure Kinect
![](screenshot.png)

This example uses the depth and color data from the Azure Kinect to shape a vector of OF meshes. The aesthetic was inspired by early video experiments. Note that this example uses the fish-eye perspective of the depth camera.

