#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxAzureKinect.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void exit();

		ofEasyCam camera;

		ofxPanel gui;
		ofxSlider  <float> depth;
		vector<ofMesh> lines;
		ofMesh testLine;
		ofPixels colorFrame;

private:
	ofxAzureKinect::Device kinectDevice;

		
};
