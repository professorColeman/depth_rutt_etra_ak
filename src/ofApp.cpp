#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//webcam.setup(800, 600);

	//These several lines setup and start the Kinect

	ofLogNotice(__FUNCTION__) << "Found " << ofxAzureKinect::Device::getInstalledCount() << " installed devices.";

	auto kinectSettings = ofxAzureKinect::DeviceSettings(); //this lets us line up a bunch of settings and send then to the camera in bulk
	kinectSettings.updateIr = false;
	kinectSettings.updateColor = true;
	kinectSettings.updateWorld = true;
	kinectSettings.colorResolution = K4A_COLOR_RESOLUTION_1080P;
	kinectSettings.depthMode =K4A_DEPTH_MODE_WFOV_UNBINNED; //this will bring in a 1024x1024 depth image
	kinectSettings.cameraFps = K4A_FRAMES_PER_SECOND_15; //for this high rez depth, we can only do 15fps
	kinectSettings.updateVbo = false; //we do not need to calculate the pointcloud since we are not using it
	if (this->kinectDevice.open(kinectSettings))
	{
		this->kinectDevice.startCameras();
	}


	
	colorFrame.allocate(1024, 1024, OF_PIXELS_RGBA); //we will use this to store the color info from the camera

	gui.setup();
	gui.add(depth.setup("Z-Depth", 1000.0, 0, 6000));//make a slider to limit the distance

	

	camera.setOrientation(glm::vec3(0, -180, 180)); //set the initial position and orientation for the camera
	camera.setPosition(400, 300, -600);

	
	//here we setup our vector of ofMeshes to make the lines
	//we will have 102 lines with 102 points each (to match the 1024x1024 resolution)
	for (int j = 0; j < 102;j++) {
		ofMesh line;
		line.setMode(OF_PRIMITIVE_LINES);

		line.addVertex(glm::vec3(0, j*10, 0)); //add the first vertex manually
		line.addColor(ofColor(255, 255, 255));

		for (size_t i = 1; i < 102; i++)
		{
			line.addVertex(glm::vec3(i * 10, j*10, 0));
			line.addColor(ofColor(255, 255, 255)); //starting color is white
			line.addIndex(i - 1); //connect every vertex to the previous one
			line.addIndex(i);
		}
		lines.push_back(line); //add this line to the lines vector
	}
	
}

//--------------------------------------------------------------
void ofApp::update(){

	if (this->kinectDevice.isStreaming()) {
		if (this->kinectDevice.getDepthPix().isAllocated())
		{
			auto const frame = (this->kinectDevice.getDepthPix()); //get our depth info
			colorFrame = (this->kinectDevice.getColorInDepthPix()); //get the color info, but in the same format/lined up with the depth info


			int index = 0; //variable to track which line we are on
			for (auto&& line : lines) { //go thru all the lines
				for (size_t i = 0; i < 102; i++) //then go thru all the points
				{
					uint16_t pixel = frame[((index * 1024 * 10) + i * 10)]; //grab the depth data at this point on this line
					ofColor cPixel = colorFrame.getColor(4*((index * 1024 *10) + i * 10)); //grab the color at this point on this line. the 4x multiplier is because there are 4 channels, RGBA
					if (pixel > depth) {
						pixel = depth; //this cleans up the result by adjusting the max depth with the slider
					}else if(pixel < 1)pixel = depth; //also clean up the data for things too close or returning a depth of zero
					line.setVertex(i, glm::vec3(i * 10, index * 10, pixel)); //set the new position using the depth
					line.setColor(i, ofColor(cPixel.b, cPixel.g, cPixel.r, cPixel.a)); //set the new color
				}
				index++; 
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofEnableDepthTest();
	camera.begin();
	glLineWidth(3);
	for (auto&& line : lines) {
		line.drawWireframe(); //draw the lines
	}
	camera.end();
	ofDisableDepthTest();

	camera.draw();

	ofSetColor(255);
	gui.draw(); 
}

//--------------------------------------------------------------
//--------------------------------------------------------------
void ofApp::exit()
{
	this->kinectDevice.close();
}
